package cl.ubb.Stack;

import static org.junit.Assert.*;

import org.junit.Test;


public class StackTest {
	
	@Test
	public void NuevaStackesvacia(){
		/*arrange*/
		Stack s = new Stack();
		int resultado;
		
		/*act*/
		resultado = s.Newstack();
		
		/*assert*/
		assertEquals(resultado,0);
	
		}
	
	@Test 
	public void AgregarUnoStackNoEsVacia(){
		/*arrange*/
		Stack s= new Stack();
		int resultado;
		
		/*act*/
		s.Agregar(1);
		resultado=s.Esvacia();
		
		/*assert*/
		assertEquals(resultado,1);
		
	}
	@Test
	public void AgregarUnoYDosTama�oDeStackEs2(){
		/*arrange*/
		Stack s= new Stack();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.Tama�o();
		/*assert*/
		assertEquals(resultado,2);
	}
	@Test 
	public void AgregarNumeroUnoHacerPopYObtenerUno(){
		/*arrange*/
		Stack s= new Stack();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,1);
	}
	@Test
	public void AgregarNumeroUnoydosHacerPopYObtenerDOS(){
		/*arrange*/
		Stack s= new Stack();
		int resultado;
		/*Act*/
		s.Newstack();
		s.Agregar(1);
		s.Agregar(2);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,2);
		}
	@Test
	public void AgregarNumeroTresYCuatroHacerPopDosVecesYObtenerTresYCuatro()
	{
		/*arrange*/
		Stack s= new Stack();
		int resultado,resultado2;
		/*Act*/
		s.Newstack();
		s.Agregar(3);
		s.Agregar(4);
		resultado=s.Pop();
		/*assert*/
		assertEquals(resultado,4);
		resultado2=s.Pop();
		assertEquals(resultado2,3);
		
	}
	


		
	}

